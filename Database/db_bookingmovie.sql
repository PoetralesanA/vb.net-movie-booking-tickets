-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 01, 2020 at 04:25 PM
-- Server version: 5.7.17-log
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bookingmovie`
--
CREATE DATABASE IF NOT EXISTS `db_bookingmovie` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_bookingmovie`;

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `id` int(11) NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`id`, `username`, `password`) VALUES
(1, 'bukanadmin', '9deb1bc75dc82f7d7099a49e64982233860db608');

-- --------------------------------------------------------

--
-- Table structure for table `tblmovie`
--

CREATE TABLE `tblmovie` (
  `idmovie` char(11) NOT NULL,
  `movie` varchar(50) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `book_count` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblmovie`
--

INSERT INTO `tblmovie` (`idmovie`, `movie`, `price`, `book_count`, `date`) VALUES
('MV_01', 'Spartan 2012', '20000', 1, '2020-07-29'),
('MV_02', 'Mr.Robot', '30000', 1, '2020-07-29'),
('MV_03', 'Mr.Robot Season 2', '35000', 0, '2020-07-29'),
('MV_04', 'Gintama The Movie', '50000', 0, '2020-07-29'),
('MV_05', 'Boruto The Movie', '32000', 1, '2020-07-29'),
('MV_06', 'Sonic The Hedgehog Film', '24000', 1, '2020-07-29'),
('MV_07', 'Dora And The Lost City Of Gold', '26500', 0, '2020-07-29'),
('MV_08', 'Bloodshot', '50000', 1, '2020-07-29'),
('MV_09', 'Gintama: The Movie: The Final', '100000', 2, '2020-07-29'),
('MV_10', 'Guardians Of The Galaxy', '130000', 0, '2020-07-29'),
('MV_11', 'Ninja Shadow Of A Tear', '11000', 1, '2020-07-29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_book`
--

CREATE TABLE `tbl_book` (
  `idbook` char(50) NOT NULL,
  `booking_name` varchar(40) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `moviename` varchar(40) NOT NULL,
  `food` varchar(40) NOT NULL,
  `price` char(40) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_book`
--

INSERT INTO `tbl_book` (`idbook`, `booking_name`, `gender`, `phone`, `moviename`, `food`, `price`, `date`) VALUES
('BMV_001', 'POETRALESANA', 'Male', '085656219405', 'Gintama: The Movie: The Final', 'Popcorn', '100000', '2020-07-29'),
('BMV_002', 'RIZALDI SALIM', 'Male', '081827287321', 'Mr.Robot', 'Hot Dog', '30000', '2020-07-29'),
('BMV_003', 'AFAT RASTA', 'Male', '091982212', 'Ninja Shadow Of A Tear', 'Coklat', '11000', '2020-07-29'),
('BMV_004', 'DEWI ISMORO', 'Female', '0910923434', 'Sonic The Hedgehog Film', 'Fish Ball', '24000', '2020-07-29'),
('BMV_005', 'HAYABUSA', 'Male', '0872618722334', 'Spartan 2012', 'Popcorn', '20000', '2020-07-29'),
('BMV_006', 'FITERI TOMBOY', 'Female', '081237873434', 'Boruto The Movie', 'Fish Ball', '32000', '2020-07-29'),
('BMV_007', 'ORLAN', 'Male', '092719728323', 'Bloodshot', 'Coklat', '50000', '2020-07-29'),
('BMV_008', 'RENALDO', 'Male', '092718727374', 'Gintama: The Movie: The Final', 'Coklat', '100000', '2020-08-02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblmovie`
--
ALTER TABLE `tblmovie`
  ADD PRIMARY KEY (`idmovie`,`movie`),
  ADD UNIQUE KEY `movie` (`movie`),
  ADD UNIQUE KEY `idmovie` (`idmovie`);

--
-- Indexes for table `tbl_book`
--
ALTER TABLE `tbl_book`
  ADD PRIMARY KEY (`idbook`),
  ADD KEY `moviename` (`moviename`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_book`
--
ALTER TABLE `tbl_book`
  ADD CONSTRAINT `tbl_book_ibfk_1` FOREIGN KEY (`moviename`) REFERENCES `tblmovie` (`movie`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

﻿Imports MySql.Data.MySqlClient
<Obsolete("", True)>
Module CRUD
#Region "Set connection"
    '-----------------------------------------------------------------------------------------'
    '                                 A T U R K O N E K S I
    '-----------------------------------------------------------------------------------------'
    Dim App_server As String = "127.0.0.1"
    Dim App_username As String = "root"
    Dim App_password As String = "mastah1337"
    Dim App_database As String = "db_bookingmovie"
    Dim App_driverODBC As String = "{MySQL ODBC 8.0 ANSI Driver}"
    '-----------------------------------------------------------------------------------------'
    '-----------------------------------------------------------------------------------------'
#End Region
#Region "CRUD"
    Private Stringconnection As String = "server=" & App_server & ";username=" & App_username & ";password=" & App_password & ";database=" & App_database & ";"
    '-----------------------------------------------------------------------------------'
    '                                   C R U D
    '-----------------------------------------------------------------------------------'
    Public Function Save_Data(ByVal Query As String)
        Dim parameter As Boolean = False
        Try
            Using conn As New MySqlConnection(Stringconnection)
                Using cmnd As New MySqlCommand(Query, conn)
                    conn.Open()
                    cmnd.ExecuteNonQuery()
                    parameter = True
                    conn.Close()
                End Using
            End Using
        Catch ex As Exception
            If ex.Message.Contains("Duplicate entry") Then
                MessageBox.Show(homepage, "Gagal menambahkan" & vbNewLine & "Data Sudah Ada!", "[Duplicate Data]", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                MessageBox.Show(homepage, ex.Message, "Terjadi Kesalahan!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Try
        Return parameter
    End Function

    Public Function Update_Data(ByVal Query As String)
        Dim parameter As Boolean = False
        Try
            Using conn As New MySqlConnection(Stringconnection)
                Using cmnd As New MySqlCommand(Query, conn)
                    conn.Open()
                    Dim i As Integer = cmnd.ExecuteNonQuery
                    If (i > 0) Then
                        parameter = True 'Update Sukses
                    Else
                        parameter = False 'Update Gagal
                    End If
                    conn.Close()
                    'If readDT.HasRows Then
                    '    parameter = True
                    'Else
                    '    parameter = False
                    '    MessageBox.Show(mainform, "[GAGAL!!]", "Data Tidak Ditemukan!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'End If
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(homepage, ex.Message, "Terjadi Kesalahan!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Return parameter
    End Function
    Public Function AppLogin(ByVal Query As String)
        Dim parameter As Boolean = False
        Try
            Using conn As New MySqlConnection(Stringconnection)
                Using cmnd As New MySqlCommand(Query, conn)
                    conn.Open()
                    Dim i As Integer = cmnd.ExecuteScalar()
                    If (i > 0) Then
                        parameter = True
                    Else
                        parameter = False
                    End If
                    conn.Close()
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(homepage, ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return parameter
    End Function
    Function Delete(QUERY) As Boolean
        Dim CheckCRUD As Boolean = False
        Using conn As New MySqlConnection(Stringconnection)
            Using cmnd As New MySqlCommand(QUERY, conn)
                Try

                    conn.Open()
                    Dim i As Integer = cmnd.ExecuteNonQuery
                    If (i > 0) Then
                        CheckCRUD = True
                    Else
                        CheckCRUD = False
                    End If
                    conn.Close()
                Catch ex As Exception
                    MessageBox.Show(homepage, ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            End Using
        End Using
        Return CheckCRUD
    End Function
    Public Function Search_data(ByVal Text As String, ByVal DatagridData As DataGridView)
        Dim datanotfound As Boolean = False
        Try
            Dim getRow As DataGridViewRow = (
            From row As DataGridViewRow In DatagridData.Rows
            From c As DataGridViewCell In row.Cells
            Where (LCase(c.FormattedValue)).ToString.Contains(LCase(Text)) Select row
            ).First

            DatagridData.Rows(getRow.Index).Selected = True
        Catch ex As Exception
            datanotfound = True
            MsgBox(IIf(ex.Message.Contains("Sequence"), "Data not found.", ex.Message), MsgBoxStyle.Exclamation, "")
        End Try
        Return datanotfound
    End Function
    Public Function View(ByVal tablename) As DataTable
        Try
            Using conn As New MySqlConnection(Stringconnection)
                Using cmnd As New MySqlCommand("SELECT * FROM " & tablename & "", conn)
                    Using DT As New DataTable
                        conn.Open()
                        DT.Load(cmnd.ExecuteReader)
                        conn.Close()
                        Return DT
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(homepage, ex.Message, "Terjadi Kesalahan!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Return Nothing
    End Function
    Public Function SelectDataTableWHERE(ByVal tableDBname As String, ByVal ColumnSelect As String, ByVal WhereValueCOMBOBOX As String, ByVal SENDVALUEDATA As String)
        'SELECT VERSION 2 [MY GOOD CODE]

        'Letakan Pada Event Index Change
        'Ini adalah pemilihan berdasar ID
        Dim sendValue As String = String.Empty
        Try
            Dim rows As DataRow = CRUD.View(tableDBname).Select("" & ColumnSelect & " = '" & WhereValueCOMBOBOX & "'").FirstOrDefault
            If Not rows Is Nothing Then
                sendValue = rows.Item(SENDVALUEDATA)
            End If
        Catch ex As Exception
            MessageBox.Show(homepage, ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return sendValue
    End Function
#End Region
    Public Function TotalBooking(namefilm) As Integer
        Dim Text As String = String.Empty
        Try
            Using conn As New MySqlConnection(Stringconnection)
                Using Cmnd As New MySqlCommand("SELECT COUNT(*) FROM tbl_book WHERE moviename = '" & namefilm & "'", conn)
                    conn.Open()
                    Text = CInt(Cmnd.ExecuteScalar()) - 1
                    conn.Close()
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(homepage, ex.Message, "Terjadi Kesalahan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return Text
    End Function
    Public Function CountBookingToday(ByVal gender) As Integer
        Dim Text As String = String.Empty
        Try
            Using conn As New MySqlConnection(Stringconnection)
                Using Cmnd As New MySqlCommand("SELECT COUNT(*) FROM tbl_book WHERE gender = '" & gender & "' AND date='" & InsertDateToDB(Now.Date) & "'", conn)
                    conn.Open()
                    Text = Cmnd.ExecuteScalar
                    conn.Close()
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(homepage, ex.Message, "Terjadi Kesalahan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
        Return Text
    End Function
End Module
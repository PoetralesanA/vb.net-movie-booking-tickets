﻿<Obsolete>
Module modfunction
    Function modEnter(ByVal e As KeyPressEventArgs) As Boolean
        If Asc(e.KeyChar) = Keys.Enter Then
            e.Handled = True
            Return True
        Else
            Return False
        End If
    End Function
    Function InsertDateToDB(ByVal value As String)
        Return Format(Convert.ToDateTime(value), "yyyy-MM-dd")
    End Function
    Public Function CheckTextKosong(ByVal mycontrol As Control, ByVal yourtag As String)
        Dim c As Boolean = True
        For Each StripButton As Control In mycontrol.Controls
            If StripButton.Text = String.Empty And StripButton.Tag = yourtag Then
                c = False
            End If
        Next
        Return c
    End Function
    'Fungsi Untuk Menghapus Seluruh Data
    Public Sub ClearAllControls(ByVal yourCtrlOBJ As Control, ByVal yourtag As String)
        For Each clearT In yourCtrlOBJ.Controls
            If clearT.Tag = yourtag Then
                clearT.Text = String.Empty
            End If
        Next
    End Sub
    Public Sub NumbericOnly(ByVal e As KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) And Not e.KeyChar = ChrW(Keys.Back) Then
            e.Handled = True
        End If
    End Sub

    'Atur Datagrid
    Public Sub PropertiesDatagird(ByVal Grid As DataGridView)
        With Grid
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False
            .MultiSelect = False
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .ReadOnly = True
        End With
    End Sub

    Public Function validasiGridSelect(ByVal datagrid As DataGridView) As Boolean
        Dim sendresult As Boolean = False
        If datagrid.RowCount = 0 Then
            MessageBox.Show("Empty row")
        Else
            If datagrid.CurrentRow.Selected = True Then
                sendresult = True
            Else
                sendresult = False
                MessageBox.Show(homepage, "Data not selected", "", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If
        Return sendresult

    End Function
    Public Sub visitedlink(ByVal link As String, ByVal caption_msg As String, ByVal title_msg As String)
        '//Validasi
        If MessageBox.Show(
            caption_msg, title_msg, MessageBoxButtons.YesNo, MessageBoxIcon.Question
            ) = DialogResult.Yes Then
            '//Visite Link
            Try
                Process.Start(link)
            Catch ex As Exception
                '//open iexplore
                Process.Start("iexplore", link)
            End Try
        End If
    End Sub
#Region "MetroFramework Open Form"
    Public Sub MetroFrameworkOpenForm(ByVal _form As Form)
        _form.ShowDialog()
        _form.Close()
    End Sub
    Public Sub MetroFrameworkShowClose(ByVal formshow As Form, ByVal formclose As Form)
        formshow.Show()
        formclose.Dispose()
        'formclose.Close()
    End Sub
#End Region
End Module

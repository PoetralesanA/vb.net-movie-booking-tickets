﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<Obsolete()>
Partial Class homepage
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(homepage))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.MetroTabControl1 = New MetroFramework.Controls.MetroTabControl()
        Me.MetroTabPage1 = New MetroFramework.Controls.MetroTabPage()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.ord_topmovie = New MetroFramework.Controls.MetroLabel()
        Me.ord_label_total = New MetroFramework.Controls.MetroLabel()
        Me.ord_label_fml = New MetroFramework.Controls.MetroLabel()
        Me.ord_label_male = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel5 = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel4 = New MetroFramework.Controls.MetroLabel()
        Me.MetroPanel1 = New MetroFramework.Controls.MetroPanel()
        Me.lbl_count_totalbooking = New MetroFramework.Controls.MetroLabel()
        Me.lbl_count_female = New MetroFramework.Controls.MetroLabel()
        Me.lbl_count_male = New MetroFramework.Controls.MetroLabel()
        Me.MetroTabPage2 = New MetroFramework.Controls.MetroTabPage()
        Me.btn_editMovie = New MetroFramework.Controls.MetroButton()
        Me.btn_RemoveMovie = New MetroFramework.Controls.MetroButton()
        Me.btn_addMovie = New MetroFramework.Controls.MetroButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.grid_movie = New MetroFramework.Controls.MetroGrid()
        Me.MetroTabPage3 = New MetroFramework.Controls.MetroTabPage()
        Me.btn_editTicket = New MetroFramework.Controls.MetroButton()
        Me.btn_removeTicket = New MetroFramework.Controls.MetroButton()
        Me.btn_Book = New MetroFramework.Controls.MetroButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.grid_bockingtickets = New MetroFramework.Controls.MetroGrid()
        Me.MetroTabPage4 = New MetroFramework.Controls.MetroTabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtSearchCustomer = New MetroFramework.Controls.MetroTextBox()
        Me.grid_costumer = New MetroFramework.Controls.MetroGrid()
        Me.MetroTabPage5 = New MetroFramework.Controls.MetroTabPage()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.myyt = New System.Windows.Forms.PictureBox()
        Me.myfacebook = New System.Windows.Forms.PictureBox()
        Me.mygitlab = New System.Windows.Forms.PictureBox()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MetroLabel7 = New MetroFramework.Controls.MetroLabel()
        Me.lbl_nameApp = New MetroFramework.Controls.MetroLabel()
        Me.lbl_version = New MetroFramework.Controls.MetroLabel()
        Me.MetroToolTip1 = New MetroFramework.Components.MetroToolTip()
        Me.MetroTabControl1.SuspendLayout()
        Me.MetroTabPage1.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grid_movie, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.grid_bockingtickets, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.grid_costumer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MetroTabPage5.SuspendLayout()
        CType(Me.myyt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.myfacebook, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mygitlab, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage1)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage2)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage3)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage4)
        Me.MetroTabControl1.Controls.Add(Me.MetroTabPage5)
        Me.MetroTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MetroTabControl1.Location = New System.Drawing.Point(20, 60)
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 4
        Me.MetroTabControl1.Size = New System.Drawing.Size(637, 347)
        Me.MetroTabControl1.Style = MetroFramework.MetroColorStyle.Brown
        Me.MetroTabControl1.TabIndex = 0
        Me.MetroTabControl1.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabControl1.UseSelectable = True
        '
        'MetroTabPage1
        '
        Me.MetroTabPage1.Controls.Add(Me.PictureBox7)
        Me.MetroTabPage1.Controls.Add(Me.PictureBox6)
        Me.MetroTabPage1.Controls.Add(Me.PictureBox5)
        Me.MetroTabPage1.Controls.Add(Me.PictureBox3)
        Me.MetroTabPage1.Controls.Add(Me.PictureBox2)
        Me.MetroTabPage1.Controls.Add(Me.PictureBox4)
        Me.MetroTabPage1.Controls.Add(Me.ord_topmovie)
        Me.MetroTabPage1.Controls.Add(Me.ord_label_total)
        Me.MetroTabPage1.Controls.Add(Me.ord_label_fml)
        Me.MetroTabPage1.Controls.Add(Me.ord_label_male)
        Me.MetroTabPage1.Controls.Add(Me.MetroLabel5)
        Me.MetroTabPage1.Controls.Add(Me.MetroLabel4)
        Me.MetroTabPage1.Controls.Add(Me.MetroPanel1)
        Me.MetroTabPage1.Controls.Add(Me.lbl_count_totalbooking)
        Me.MetroTabPage1.Controls.Add(Me.lbl_count_female)
        Me.MetroTabPage1.Controls.Add(Me.lbl_count_male)
        Me.MetroTabPage1.HorizontalScrollbarBarColor = True
        Me.MetroTabPage1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.HorizontalScrollbarSize = 10
        Me.MetroTabPage1.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage1.Name = "MetroTabPage1"
        Me.MetroTabPage1.Size = New System.Drawing.Size(629, 305)
        Me.MetroTabPage1.TabIndex = 0
        Me.MetroTabPage1.Text = "HOME"
        Me.MetroTabPage1.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabPage1.VerticalScrollbarBarColor = True
        Me.MetroTabPage1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage1.VerticalScrollbarSize = 10
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(0, 267)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(26, 22)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 17
        Me.PictureBox7.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(3, 209)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(23, 22)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 16
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(3, 238)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(23, 22)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 15
        Me.PictureBox5.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(317, 29)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(29, 29)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 13
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(0, 29)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(29, 29)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 12
        Me.PictureBox2.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(0, 77)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(29, 29)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 14
        Me.PictureBox4.TabStop = False
        '
        'ord_topmovie
        '
        Me.ord_topmovie.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ord_topmovie.Location = New System.Drawing.Point(317, 209)
        Me.ord_topmovie.Name = "ord_topmovie"
        Me.ord_topmovie.Size = New System.Drawing.Size(313, 90)
        Me.ord_topmovie.TabIndex = 7
        Me.ord_topmovie.Text = "- ######### (COUNT)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "- ######### (COUNT)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "- ######### (COUNT)"
        Me.ord_topmovie.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.ord_topmovie.UseCustomForeColor = True
        '
        'ord_label_total
        '
        Me.ord_label_total.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ord_label_total.Location = New System.Drawing.Point(32, 267)
        Me.ord_label_total.Name = "ord_label_total"
        Me.ord_label_total.Size = New System.Drawing.Size(273, 29)
        Me.ord_label_total.TabIndex = 5
        Me.ord_label_total.Text = "Total : ######"
        Me.ord_label_total.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.ord_label_total.UseCustomForeColor = True
        '
        'ord_label_fml
        '
        Me.ord_label_fml.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ord_label_fml.Location = New System.Drawing.Point(32, 238)
        Me.ord_label_fml.Name = "ord_label_fml"
        Me.ord_label_fml.Size = New System.Drawing.Size(285, 29)
        Me.ord_label_fml.TabIndex = 4
        Me.ord_label_fml.Text = "Female : ######"
        Me.ord_label_fml.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.ord_label_fml.UseCustomForeColor = True
        '
        'ord_label_male
        '
        Me.ord_label_male.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ord_label_male.Location = New System.Drawing.Point(32, 209)
        Me.ord_label_male.Name = "ord_label_male"
        Me.ord_label_male.Size = New System.Drawing.Size(285, 29)
        Me.ord_label_male.TabIndex = 3
        Me.ord_label_male.Text = "Male : ######"
        Me.ord_label_male.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.ord_label_male.UseCustomForeColor = True
        '
        'MetroLabel5
        '
        Me.MetroLabel5.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel5.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel5.ForeColor = System.Drawing.Color.LawnGreen
        Me.MetroLabel5.Location = New System.Drawing.Point(317, 168)
        Me.MetroLabel5.Name = "MetroLabel5"
        Me.MetroLabel5.Size = New System.Drawing.Size(150, 29)
        Me.MetroLabel5.TabIndex = 7
        Me.MetroLabel5.Text = "TOP MOVIE"
        Me.MetroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroLabel5.UseCustomForeColor = True
        '
        'MetroLabel4
        '
        Me.MetroLabel4.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel4.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel4.ForeColor = System.Drawing.Color.LawnGreen
        Me.MetroLabel4.Location = New System.Drawing.Point(26, 168)
        Me.MetroLabel4.Name = "MetroLabel4"
        Me.MetroLabel4.Size = New System.Drawing.Size(156, 29)
        Me.MetroLabel4.TabIndex = 6
        Me.MetroLabel4.Text = "ORDER TODAY"
        Me.MetroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroLabel4.UseCustomForeColor = True
        '
        'MetroPanel1
        '
        Me.MetroPanel1.BackColor = System.Drawing.Color.Gainsboro
        Me.MetroPanel1.HorizontalScrollbarBarColor = True
        Me.MetroPanel1.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.HorizontalScrollbarSize = 10
        Me.MetroPanel1.Location = New System.Drawing.Point(3, 130)
        Me.MetroPanel1.Name = "MetroPanel1"
        Me.MetroPanel1.Size = New System.Drawing.Size(622, 10)
        Me.MetroPanel1.TabIndex = 5
        Me.MetroPanel1.UseCustomBackColor = True
        Me.MetroPanel1.VerticalScrollbarBarColor = True
        Me.MetroPanel1.VerticalScrollbarHighlightOnWheel = False
        Me.MetroPanel1.VerticalScrollbarSize = 10
        '
        'lbl_count_totalbooking
        '
        Me.lbl_count_totalbooking.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.lbl_count_totalbooking.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbl_count_totalbooking.Location = New System.Drawing.Point(38, 77)
        Me.lbl_count_totalbooking.Name = "lbl_count_totalbooking"
        Me.lbl_count_totalbooking.Size = New System.Drawing.Size(599, 29)
        Me.lbl_count_totalbooking.TabIndex = 1
        Me.lbl_count_totalbooking.Text = "Total Movie Booking : #########"
        Me.lbl_count_totalbooking.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.lbl_count_totalbooking.UseCustomForeColor = True
        '
        'lbl_count_female
        '
        Me.lbl_count_female.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.lbl_count_female.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbl_count_female.Location = New System.Drawing.Point(352, 29)
        Me.lbl_count_female.Name = "lbl_count_female"
        Me.lbl_count_female.Size = New System.Drawing.Size(277, 29)
        Me.lbl_count_female.TabIndex = 2
        Me.lbl_count_female.Text = "Female : #####"
        Me.lbl_count_female.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.lbl_count_female.UseCustomForeColor = True
        '
        'lbl_count_male
        '
        Me.lbl_count_male.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.lbl_count_male.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbl_count_male.Location = New System.Drawing.Point(38, 29)
        Me.lbl_count_male.Name = "lbl_count_male"
        Me.lbl_count_male.Size = New System.Drawing.Size(285, 29)
        Me.lbl_count_male.TabIndex = 0
        Me.lbl_count_male.Text = "Male : #####"
        Me.lbl_count_male.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.lbl_count_male.UseCustomForeColor = True
        '
        'MetroTabPage2
        '
        Me.MetroTabPage2.Controls.Add(Me.btn_editMovie)
        Me.MetroTabPage2.Controls.Add(Me.btn_RemoveMovie)
        Me.MetroTabPage2.Controls.Add(Me.btn_addMovie)
        Me.MetroTabPage2.Controls.Add(Me.GroupBox1)
        Me.MetroTabPage2.HorizontalScrollbarBarColor = True
        Me.MetroTabPage2.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.HorizontalScrollbarSize = 10
        Me.MetroTabPage2.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage2.Name = "MetroTabPage2"
        Me.MetroTabPage2.Size = New System.Drawing.Size(629, 305)
        Me.MetroTabPage2.TabIndex = 1
        Me.MetroTabPage2.Text = "MOVIE"
        Me.MetroTabPage2.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabPage2.VerticalScrollbarBarColor = True
        Me.MetroTabPage2.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage2.VerticalScrollbarSize = 10
        '
        'btn_editMovie
        '
        Me.btn_editMovie.Location = New System.Drawing.Point(268, 257)
        Me.btn_editMovie.Name = "btn_editMovie"
        Me.btn_editMovie.Size = New System.Drawing.Size(116, 37)
        Me.btn_editMovie.Style = MetroFramework.MetroColorStyle.Green
        Me.btn_editMovie.TabIndex = 2
        Me.btn_editMovie.Text = "Edit Movie"
        Me.btn_editMovie.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.btn_editMovie.UseSelectable = True
        '
        'btn_RemoveMovie
        '
        Me.btn_RemoveMovie.Location = New System.Drawing.Point(146, 257)
        Me.btn_RemoveMovie.Name = "btn_RemoveMovie"
        Me.btn_RemoveMovie.Size = New System.Drawing.Size(116, 37)
        Me.btn_RemoveMovie.Style = MetroFramework.MetroColorStyle.Green
        Me.btn_RemoveMovie.TabIndex = 1
        Me.btn_RemoveMovie.Text = "Remove Movie"
        Me.btn_RemoveMovie.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.btn_RemoveMovie.UseSelectable = True
        '
        'btn_addMovie
        '
        Me.btn_addMovie.Location = New System.Drawing.Point(24, 257)
        Me.btn_addMovie.Name = "btn_addMovie"
        Me.btn_addMovie.Size = New System.Drawing.Size(116, 37)
        Me.btn_addMovie.Style = MetroFramework.MetroColorStyle.Green
        Me.btn_addMovie.TabIndex = 0
        Me.btn_addMovie.Text = "Add Movie"
        Me.btn_addMovie.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.btn_addMovie.UseSelectable = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.grid_movie)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(10, 10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(608, 241)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "VIEW"
        '
        'grid_movie
        '
        Me.grid_movie.AllowUserToResizeRows = False
        Me.grid_movie.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.grid_movie.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grid_movie.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.grid_movie.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(173, Byte), Integer))
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(206, Byte), Integer))
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grid_movie.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.grid_movie.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(206, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grid_movie.DefaultCellStyle = DataGridViewCellStyle2
        Me.grid_movie.EnableHeadersVisualStyles = False
        Me.grid_movie.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.grid_movie.GridColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.grid_movie.Location = New System.Drawing.Point(14, 27)
        Me.grid_movie.Name = "grid_movie"
        Me.grid_movie.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(173, Byte), Integer))
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(201, Byte), Integer), CType(CType(206, Byte), Integer))
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grid_movie.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.grid_movie.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.grid_movie.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grid_movie.Size = New System.Drawing.Size(581, 197)
        Me.grid_movie.Style = MetroFramework.MetroColorStyle.Teal
        Me.grid_movie.TabIndex = 0
        Me.grid_movie.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MetroTabPage3
        '
        Me.MetroTabPage3.Controls.Add(Me.btn_editTicket)
        Me.MetroTabPage3.Controls.Add(Me.btn_removeTicket)
        Me.MetroTabPage3.Controls.Add(Me.btn_Book)
        Me.MetroTabPage3.Controls.Add(Me.GroupBox2)
        Me.MetroTabPage3.HorizontalScrollbarBarColor = True
        Me.MetroTabPage3.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage3.HorizontalScrollbarSize = 10
        Me.MetroTabPage3.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage3.Name = "MetroTabPage3"
        Me.MetroTabPage3.Size = New System.Drawing.Size(629, 305)
        Me.MetroTabPage3.TabIndex = 2
        Me.MetroTabPage3.Text = "BOOKING TICKET"
        Me.MetroTabPage3.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabPage3.VerticalScrollbarBarColor = True
        Me.MetroTabPage3.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage3.VerticalScrollbarSize = 10
        '
        'btn_editTicket
        '
        Me.btn_editTicket.Location = New System.Drawing.Point(268, 257)
        Me.btn_editTicket.Name = "btn_editTicket"
        Me.btn_editTicket.Size = New System.Drawing.Size(116, 37)
        Me.btn_editTicket.Style = MetroFramework.MetroColorStyle.Green
        Me.btn_editTicket.TabIndex = 2
        Me.btn_editTicket.Text = "Edit Data"
        Me.btn_editTicket.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.btn_editTicket.UseSelectable = True
        '
        'btn_removeTicket
        '
        Me.btn_removeTicket.Location = New System.Drawing.Point(146, 257)
        Me.btn_removeTicket.Name = "btn_removeTicket"
        Me.btn_removeTicket.Size = New System.Drawing.Size(116, 37)
        Me.btn_removeTicket.Style = MetroFramework.MetroColorStyle.Green
        Me.btn_removeTicket.TabIndex = 1
        Me.btn_removeTicket.Text = "Remove"
        Me.btn_removeTicket.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.btn_removeTicket.UseSelectable = True
        '
        'btn_Book
        '
        Me.btn_Book.Location = New System.Drawing.Point(24, 257)
        Me.btn_Book.Name = "btn_Book"
        Me.btn_Book.Size = New System.Drawing.Size(116, 37)
        Me.btn_Book.Style = MetroFramework.MetroColorStyle.Green
        Me.btn_Book.TabIndex = 0
        Me.btn_Book.Text = "Book"
        Me.btn_Book.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.btn_Book.UseSelectable = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.grid_bockingtickets)
        Me.GroupBox2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(10, 10)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(608, 241)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "VIEW"
        '
        'grid_bockingtickets
        '
        Me.grid_bockingtickets.AllowUserToResizeRows = False
        Me.grid_bockingtickets.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.grid_bockingtickets.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grid_bockingtickets.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.grid_bockingtickets.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(124, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(153, Byte), Integer))
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(140, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(173, Byte), Integer))
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grid_bockingtickets.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.grid_bockingtickets.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(140, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(173, Byte), Integer))
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grid_bockingtickets.DefaultCellStyle = DataGridViewCellStyle5
        Me.grid_bockingtickets.EnableHeadersVisualStyles = False
        Me.grid_bockingtickets.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.grid_bockingtickets.GridColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.grid_bockingtickets.Location = New System.Drawing.Point(14, 27)
        Me.grid_bockingtickets.Name = "grid_bockingtickets"
        Me.grid_bockingtickets.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(CType(CType(124, Byte), Integer), CType(CType(65, Byte), Integer), CType(CType(153, Byte), Integer))
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(140, Byte), Integer), CType(CType(73, Byte), Integer), CType(CType(173, Byte), Integer))
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grid_bockingtickets.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.grid_bockingtickets.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.grid_bockingtickets.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grid_bockingtickets.Size = New System.Drawing.Size(581, 197)
        Me.grid_bockingtickets.Style = MetroFramework.MetroColorStyle.Purple
        Me.grid_bockingtickets.TabIndex = 0
        Me.grid_bockingtickets.TabStop = False
        Me.grid_bockingtickets.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MetroTabPage4
        '
        Me.MetroTabPage4.Controls.Add(Me.GroupBox3)
        Me.MetroTabPage4.HorizontalScrollbarBarColor = True
        Me.MetroTabPage4.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage4.HorizontalScrollbarSize = 10
        Me.MetroTabPage4.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage4.Name = "MetroTabPage4"
        Me.MetroTabPage4.Size = New System.Drawing.Size(629, 305)
        Me.MetroTabPage4.TabIndex = 3
        Me.MetroTabPage4.Text = "CUSTOMER"
        Me.MetroTabPage4.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabPage4.VerticalScrollbarBarColor = True
        Me.MetroTabPage4.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage4.VerticalScrollbarSize = 10
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.txtSearchCustomer)
        Me.GroupBox3.Controls.Add(Me.grid_costumer)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.White
        Me.GroupBox3.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(629, 305)
        Me.GroupBox3.TabIndex = 14
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "VIEW"
        '
        'txtSearchCustomer
        '
        Me.txtSearchCustomer.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        '
        '
        '
        Me.txtSearchCustomer.CustomButton.Image = Nothing
        Me.txtSearchCustomer.CustomButton.Location = New System.Drawing.Point(305, 1)
        Me.txtSearchCustomer.CustomButton.Name = ""
        Me.txtSearchCustomer.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtSearchCustomer.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtSearchCustomer.CustomButton.TabIndex = 1
        Me.txtSearchCustomer.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtSearchCustomer.CustomButton.UseSelectable = True
        Me.txtSearchCustomer.CustomButton.Visible = False
        Me.txtSearchCustomer.ForeColor = System.Drawing.Color.Transparent
        Me.txtSearchCustomer.Lines = New String(-1) {}
        Me.txtSearchCustomer.Location = New System.Drawing.Point(14, 272)
        Me.txtSearchCustomer.MaxLength = 32767
        Me.txtSearchCustomer.Name = "txtSearchCustomer"
        Me.txtSearchCustomer.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtSearchCustomer.PromptText = "Search....."
        Me.txtSearchCustomer.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtSearchCustomer.SelectedText = ""
        Me.txtSearchCustomer.SelectionLength = 0
        Me.txtSearchCustomer.SelectionStart = 0
        Me.txtSearchCustomer.ShortcutsEnabled = True
        Me.txtSearchCustomer.Size = New System.Drawing.Size(327, 23)
        Me.txtSearchCustomer.TabIndex = 1
        Me.txtSearchCustomer.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.txtSearchCustomer.UseCustomBackColor = True
        Me.txtSearchCustomer.UseSelectable = True
        Me.txtSearchCustomer.WaterMark = "Search....."
        Me.txtSearchCustomer.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtSearchCustomer.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'grid_costumer
        '
        Me.grid_costumer.AllowUserToResizeRows = False
        Me.grid_costumer.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.grid_costumer.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.grid_costumer.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.grid_costumer.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(89, Byte), Integer))
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(104, Byte), Integer))
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grid_costumer.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.grid_costumer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(104, Byte), Integer))
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grid_costumer.DefaultCellStyle = DataGridViewCellStyle8
        Me.grid_costumer.EnableHeadersVisualStyles = False
        Me.grid_costumer.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.grid_costumer.GridColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        Me.grid_costumer.Location = New System.Drawing.Point(10, 27)
        Me.grid_costumer.Name = "grid_costumer"
        Me.grid_costumer.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(177, Byte), Integer), CType(CType(89, Byte), Integer))
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Segoe UI", 11.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(208, Byte), Integer), CType(CType(104, Byte), Integer))
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer), CType(CType(17, Byte), Integer))
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.grid_costumer.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.grid_costumer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.grid_costumer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grid_costumer.Size = New System.Drawing.Size(609, 239)
        Me.grid_costumer.Style = MetroFramework.MetroColorStyle.Green
        Me.grid_costumer.TabIndex = 0
        Me.grid_costumer.TabStop = False
        Me.grid_costumer.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'MetroTabPage5
        '
        Me.MetroTabPage5.Controls.Add(Me.MetroLabel1)
        Me.MetroTabPage5.Controls.Add(Me.myyt)
        Me.MetroTabPage5.Controls.Add(Me.myfacebook)
        Me.MetroTabPage5.Controls.Add(Me.mygitlab)
        Me.MetroTabPage5.Controls.Add(Me.LinkLabel1)
        Me.MetroTabPage5.Controls.Add(Me.PictureBox1)
        Me.MetroTabPage5.Controls.Add(Me.MetroLabel7)
        Me.MetroTabPage5.Controls.Add(Me.lbl_nameApp)
        Me.MetroTabPage5.Controls.Add(Me.lbl_version)
        Me.MetroTabPage5.HorizontalScrollbarBarColor = True
        Me.MetroTabPage5.HorizontalScrollbarHighlightOnWheel = False
        Me.MetroTabPage5.HorizontalScrollbarSize = 10
        Me.MetroTabPage5.Location = New System.Drawing.Point(4, 38)
        Me.MetroTabPage5.Name = "MetroTabPage5"
        Me.MetroTabPage5.Size = New System.Drawing.Size(629, 305)
        Me.MetroTabPage5.TabIndex = 4
        Me.MetroTabPage5.Text = "ABOUT"
        Me.MetroTabPage5.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabPage5.UseVisualStyleBackColor = True
        Me.MetroTabPage5.VerticalScrollbarBarColor = True
        Me.MetroTabPage5.VerticalScrollbarHighlightOnWheel = False
        Me.MetroTabPage5.VerticalScrollbarSize = 10
        '
        'MetroLabel1
        '
        Me.MetroLabel1.BackColor = System.Drawing.Color.Black
        Me.MetroLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.MetroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.MetroLabel1.ForeColor = System.Drawing.Color.White
        Me.MetroLabel1.Location = New System.Drawing.Point(3, 241)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(68, 20)
        Me.MetroLabel1.TabIndex = 10
        Me.MetroLabel1.Text = "Follow : "
        Me.MetroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.MetroLabel1.Theme = MetroFramework.MetroThemeStyle.Light
        Me.MetroLabel1.UseCustomBackColor = True
        Me.MetroLabel1.UseCustomForeColor = True
        '
        'myyt
        '
        Me.myyt.BackColor = System.Drawing.Color.Transparent
        Me.myyt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.myyt.Image = CType(resources.GetObject("myyt.Image"), System.Drawing.Image)
        Me.myyt.Location = New System.Drawing.Point(87, 264)
        Me.myyt.Name = "myyt"
        Me.myyt.Size = New System.Drawing.Size(36, 38)
        Me.myyt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.myyt.TabIndex = 9
        Me.myyt.TabStop = False
        Me.MetroToolTip1.SetToolTip(Me.myyt, "Youtube")
        '
        'myfacebook
        '
        Me.myfacebook.BackColor = System.Drawing.Color.Transparent
        Me.myfacebook.Cursor = System.Windows.Forms.Cursors.Hand
        Me.myfacebook.Image = CType(resources.GetObject("myfacebook.Image"), System.Drawing.Image)
        Me.myfacebook.Location = New System.Drawing.Point(45, 264)
        Me.myfacebook.Name = "myfacebook"
        Me.myfacebook.Size = New System.Drawing.Size(36, 38)
        Me.myfacebook.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.myfacebook.TabIndex = 8
        Me.myfacebook.TabStop = False
        Me.MetroToolTip1.SetToolTip(Me.myfacebook, "Facebook")
        '
        'mygitlab
        '
        Me.mygitlab.BackColor = System.Drawing.Color.Transparent
        Me.mygitlab.Cursor = System.Windows.Forms.Cursors.Hand
        Me.mygitlab.Image = CType(resources.GetObject("mygitlab.Image"), System.Drawing.Image)
        Me.mygitlab.Location = New System.Drawing.Point(3, 264)
        Me.mygitlab.Name = "mygitlab"
        Me.mygitlab.Size = New System.Drawing.Size(36, 38)
        Me.mygitlab.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.mygitlab.TabIndex = 7
        Me.mygitlab.TabStop = False
        Me.MetroToolTip1.SetToolTip(Me.mygitlab, "GitLab")
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.BackColor = System.Drawing.Color.Transparent
        Me.LinkLabel1.Font = New System.Drawing.Font("Book Antiqua", 9.75!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LinkLabel1.Location = New System.Drawing.Point(563, 287)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(60, 21)
        Me.LinkLabel1.TabIndex = 0
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Donate>>"
        Me.LinkLabel1.UseCompatibleTextRendering = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(258, 108)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(112, 105)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'MetroLabel7
        '
        Me.MetroLabel7.FontSize = MetroFramework.MetroLabelSize.Small
        Me.MetroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Regular
        Me.MetroLabel7.ForeColor = System.Drawing.Color.Turquoise
        Me.MetroLabel7.Location = New System.Drawing.Point(0, 76)
        Me.MetroLabel7.Name = "MetroLabel7"
        Me.MetroLabel7.Size = New System.Drawing.Size(629, 29)
        Me.MetroLabel7.TabIndex = 4
        Me.MetroLabel7.Text = "~ Develop by PoetralesanA ~"
        Me.MetroLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.MetroLabel7.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroLabel7.UseCustomForeColor = True
        '
        'lbl_nameApp
        '
        Me.lbl_nameApp.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.lbl_nameApp.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbl_nameApp.Location = New System.Drawing.Point(0, 47)
        Me.lbl_nameApp.Name = "lbl_nameApp"
        Me.lbl_nameApp.Size = New System.Drawing.Size(629, 29)
        Me.lbl_nameApp.TabIndex = 3
        Me.lbl_nameApp.Text = "nameApp()"
        Me.lbl_nameApp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbl_nameApp.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.lbl_nameApp.UseCustomForeColor = True
        '
        'lbl_version
        '
        Me.lbl_version.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbl_version.Location = New System.Drawing.Point(0, 216)
        Me.lbl_version.Name = "lbl_version"
        Me.lbl_version.Size = New System.Drawing.Size(629, 29)
        Me.lbl_version.TabIndex = 6
        Me.lbl_version.Text = "VersionApp()"
        Me.lbl_version.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbl_version.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.lbl_version.UseCustomForeColor = True
        '
        'MetroToolTip1
        '
        Me.MetroToolTip1.Style = MetroFramework.MetroColorStyle.Teal
        Me.MetroToolTip1.StyleManager = Nothing
        Me.MetroToolTip1.Theme = MetroFramework.MetroThemeStyle.Dark
        '
        'homepage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(677, 427)
        Me.Controls.Add(Me.MetroTabControl1)
        Me.MaximumSize = New System.Drawing.Size(677, 427)
        Me.Name = "homepage"
        Me.Opacity = 0.96R
        Me.Resizable = False
        Me.Style = MetroFramework.MetroColorStyle.Teal
        Me.Text = "MOVIE TICKETS BOOKING"
        Me.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroTabControl1.ResumeLayout(False)
        Me.MetroTabPage1.ResumeLayout(False)
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.grid_movie, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.grid_bockingtickets, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage4.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.grid_costumer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MetroTabPage5.ResumeLayout(False)
        Me.MetroTabPage5.PerformLayout()
        CType(Me.myyt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.myfacebook, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mygitlab, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MetroTabControl1 As MetroFramework.Controls.MetroTabControl
    Friend WithEvents MetroTabPage1 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage2 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage3 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage4 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents MetroTabPage5 As MetroFramework.Controls.MetroTabPage
    Friend WithEvents lbl_count_male As MetroFramework.Controls.MetroLabel
    Friend WithEvents lbl_count_female As MetroFramework.Controls.MetroLabel
    Friend WithEvents lbl_count_totalbooking As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroPanel1 As MetroFramework.Controls.MetroPanel
    Friend WithEvents MetroLabel5 As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel4 As MetroFramework.Controls.MetroLabel
    Friend WithEvents ord_label_fml As MetroFramework.Controls.MetroLabel
    Friend WithEvents ord_label_male As MetroFramework.Controls.MetroLabel
    Friend WithEvents ord_label_total As MetroFramework.Controls.MetroLabel
    Friend WithEvents ord_topmovie As MetroFramework.Controls.MetroLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grid_movie As MetroFramework.Controls.MetroGrid
    Friend WithEvents btn_addMovie As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_RemoveMovie As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_editMovie As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_editTicket As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_removeTicket As MetroFramework.Controls.MetroButton
    Friend WithEvents btn_Book As MetroFramework.Controls.MetroButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents grid_bockingtickets As MetroFramework.Controls.MetroGrid
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSearchCustomer As MetroFramework.Controls.MetroTextBox
    Friend WithEvents grid_costumer As MetroFramework.Controls.MetroGrid
    Friend WithEvents lbl_nameApp As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel7 As MetroFramework.Controls.MetroLabel
    Friend WithEvents lbl_version As MetroFramework.Controls.MetroLabel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents mygitlab As System.Windows.Forms.PictureBox
    Friend WithEvents myfacebook As System.Windows.Forms.PictureBox
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents myyt As System.Windows.Forms.PictureBox
    Friend WithEvents MetroToolTip1 As MetroFramework.Components.MetroToolTip
End Class

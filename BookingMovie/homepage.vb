﻿Public Class homepage
    Dim totalmoviebooking As New ranking
    Dim gender As New ranking
    Dim todayCount As New ranking


    Dim Ranking As New ranking
    Public movienamePUBLIC As String = String.Empty


    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        modfunction.MetroFrameworkOpenForm(formdnt)
    End Sub
    Private Sub btn_RemoveMovie_Click(sender As Object, e As EventArgs) Handles btn_RemoveMovie.Click
        RemoveMovie()
    End Sub

    Private Sub btn_removeTicket_Click(sender As Object, e As EventArgs) Handles btn_removeTicket.Click
        RemoveTicketBooking()
    End Sub
    Private Sub btn_addMovie_Click(sender As Object, e As EventArgs) Handles btn_addMovie.Click
        modfunction.MetroFrameworkOpenForm(formMovie_Add_and_Edit)
    End Sub

    Private Sub btn_Book_Click(sender As Object, e As EventArgs) Handles btn_Book.Click
        modfunction.MetroFrameworkOpenForm(formBooking_Add_and_Edit)
    End Sub
    Private Sub homepage_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        '//Select Tab Home
        MetroTabControl1.SelectedIndex = 0
        '//Show data movie and Show data booking ticket
        refreshdataload()

        '// Set Name, Version
        AboutLoad()

        '//Settings Properti Datagrid
        mypropertiesDatagrid()

        Loadhomedata()
    End Sub
    Private Sub btn_editTicket_Click(sender As Object, e As EventArgs) Handles btn_editTicket.Click
        If modfunction.validasiGridSelect(grid_bockingtickets) = True Then 'SelectGrid

            formBooking_Add_and_Edit.MetroButton1.Text = "Update Data"
            formBooking_Add_and_Edit.txtIdbocking.Enabled = False 'disableID
            formBooking_Add_and_Edit.cmbMoviename.Visible = False
            retrievegriddataBookingTickets()
            modfunction.MetroFrameworkOpenForm(formBooking_Add_and_Edit)
        End If
    End Sub

    Private Sub btn_editMovie_Click(sender As Object, e As EventArgs) Handles btn_editMovie.Click
        If modfunction.validasiGridSelect(grid_movie) = True Then 'SelectGrid

            formMovie_Add_and_Edit.MetroButton1.Text = "Update Data"
            formMovie_Add_and_Edit.txtIdmovie.Enabled = False 'disableID
            retrievegriddataMOVIE()
            modfunction.MetroFrameworkOpenForm(formMovie_Add_and_Edit)
        End If
    End Sub
    Private Sub RemoveTicketBooking()
        If modfunction.validasiGridSelect(grid_bockingtickets) = True Then 'SelectGrid

            If MessageBox.Show(Me, "Remove data..?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim idboking As String = String.Empty
                Dim query As String = String.Empty

                idboking = grid_bockingtickets.CurrentRow.Cells(0).Value.ToString()
                query = "DELETE FROM `tbl_book` WHERE `idbook`='" & idboking & "'"
                Select Case CRUD.Delete(query)
                    Case True
                        bookingtickets.Remove()
                        Me.refreshdataload()
                        MessageBox.Show(Me, "deleted successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Case False
                        MessageBox.Show(Me, "delete failed", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Select

            End If
        End If
    End Sub
    Private Sub RemoveMovie()
        If modfunction.validasiGridSelect(grid_movie) = True Then 'SelectGrid
            If MessageBox.Show(Me, "Remove data..?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim idmovie As String = String.Empty
                Dim query As String = String.Empty
                idmovie = grid_movie.CurrentRow.Cells(0).Value.ToString()
                query = "DELETE FROM `tblmovie` WHERE `idmovie`='" & idmovie & "'"
                Select Case CRUD.Delete(query)
                    Case True
                        refreshdataload()
                        MessageBox.Show(Me, "deleted successfully", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Case False
                        MessageBox.Show(Me, "delete failed", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Select
            End If
        End If
    End Sub
#Region "Social Media"
    Private Sub PictureBox8_Click(sender As Object, e As EventArgs) Handles mygitlab.Click
        modfunction.visitedlink(My.Resources.gitlab, "Visit GitLab?", "")
    End Sub

    Private Sub myfacebook_Click(sender As Object, e As EventArgs) Handles myfacebook.Click
        modfunction.visitedlink(My.Resources.facebook, "Visit Facebook?", "")
    End Sub

    Private Sub myyt_Click(sender As Object, e As EventArgs) Handles myyt.Click
        modfunction.visitedlink(My.Resources.youtube, "Visit YouTube?", "")
    End Sub
#End Region
#Region "Sending data on a different form"
    Private Sub retrievegriddataMOVIE()

        '/////////send data to edit movie >>

        Dim idmovie, movie, price
        idmovie = String.Empty
        movie = String.Empty
        price = String.Empty

        'get data on datagrid
        idmovie = grid_movie.CurrentRow.Cells(0).Value.ToString()
        movie = grid_movie.CurrentRow.Cells(1).Value.ToString()
        price = grid_movie.CurrentRow.Cells(2).Value.ToString()


        formMovie_Add_and_Edit.txtIdmovie.Text = idmovie
        formMovie_Add_and_Edit.txtMovieName.Text = movie
        formMovie_Add_and_Edit.txtPrice.Text = price
    End Sub
    Private Sub retrievegriddataBookingTickets()
        movienamePUBLIC = String.Empty
        '/////////send data to edit movie >>
        Dim idmovie, bookingname, gender, phone, moviename, food
        idmovie = String.Empty
        bookingname = String.Empty
        gender = String.Empty
        phone = String.Empty
        moviename = String.Empty
        food = String.Empty


        'get data on datagrid
        idmovie = grid_bockingtickets.CurrentRow.Cells(0).Value.ToString()
        bookingname = grid_bockingtickets.CurrentRow.Cells(1).Value.ToString()
        gender = grid_bockingtickets.CurrentRow.Cells(2).Value.ToString()
        phone = grid_bockingtickets.CurrentRow.Cells(3).Value.ToString()
        moviename = grid_bockingtickets.CurrentRow.Cells(4).Value.ToString()
        food = grid_bockingtickets.CurrentRow.Cells(5).Value.ToString()



        With formBooking_Add_and_Edit

            .txtIdbocking.Text = idmovie
            .txtName.Text = bookingname
            'Cheked Gender
            If gender = .rbMale.Text Then .rbMale.Checked = True Else .rbFemale.Checked = True
            .txtPhone.Text = phone
            Me.movienamePUBLIC = moviename
            .cmbSnack.SelectedItem = food
        End With
    End Sub
#End Region
    '------------------------------------------ ABOUT ------------------------------------------'
    '//Logo
    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        MessageBox.Show(Me, Me.Text & vbNewLine & "Version : " & My.Application.Info.Version.ToString & vbNewLine & "by PoetralesanA", "https://www.facebook.com/poetralesana", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
    '//Version
    Private Sub AboutLoad()
        lbl_version.Text = "Version : " & My.Application.Info.Version.ToString
        lbl_nameApp.Text = "💀 " & Me.Text & " 💀"
    End Sub


    Private Sub txtSearchCustomer_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtSearchCustomer.KeyPress

        If modfunction.modEnter(e) = True Then
            Select Case CRUD.Search_data(txtSearchCustomer.Text, grid_costumer)
                Case True
                    txtSearchCustomer.BackColor = Color.Red
                Case False
                    txtSearchCustomer.BackColor = Color.FromArgb(17, 17, 17)
            End Select
        End If
    End Sub

    Private Sub txtSearchCustomer_TextChanged(sender As Object, e As EventArgs) Handles txtSearchCustomer.TextChanged
        If txtSearchCustomer.Text.Length < 1 Then
            txtSearchCustomer.BackColor = Color.FromArgb(17, 17, 17)
        End If
    End Sub

    Private Sub MetroTabControl1_Selecting(sender As Object, e As TabControlCancelEventArgs) Handles MetroTabControl1.Selecting
        Select Case e.TabPageIndex
            Case 0 'Tab Home // Refresh Data
                Me.refreshdataload()

                Me.Loadhomedata() 'ORDER
            Case 3 'Tab Customer // Clear Text Search
                Me.txtSearchCustomer.Clear()
        End Select
    End Sub
    Private Sub Loadhomedata()
        COUNT_ALL()
        COUNT_order_today()
        TopMovieRanking()
    End Sub
    Private Sub TopMovieRanking()
        ord_topmovie.Text = Ranking.movie()
    End Sub
    Private Sub COUNT_order_today()
        '/////////////////////////////////// COUNT ORDER Now!! /////////////////////////////////// '

        '//Variable
        Dim total As Integer = 0
        Dim male As Integer = 0
        Dim female As Integer = 0

        '//Count
        male = todayCount.male()
        female = todayCount.female()
        total = male + female

        '//Result
        ord_label_male.Text = "Male Visitors : " & male
        ord_label_fml.Text = "Female Visitors : " & female
        ord_label_total.Text = "Total Visitors : " & total
        '/////////////////////////////////////////////////////////////////////////////////////////////
    End Sub
    Private Sub COUNT_ALL()
        '//Variable
        Dim int_male As Integer
        Dim int_female As Integer
        Dim totalcountdata As Integer

        '//Count
        int_male = gender.count(grid_bockingtickets, "Male")
        int_female = gender.count(grid_bockingtickets, "Female")
        totalcountdata = int_male + int_female

        '//Result
        lbl_count_male.Text = "Male Total : " & int_male & " Visitors"
        lbl_count_female.Text = "Female Total : " & int_female & " Visitors"
        lbl_count_totalbooking.Text = "Total Movie Booking : " & totalcountdata & " Audience"
    End Sub
    Private Sub mypropertiesDatagrid()
        modfunction.PropertiesDatagird(grid_movie)
        modfunction.PropertiesDatagird(grid_bockingtickets)
        modfunction.PropertiesDatagird(grid_costumer)

        '------------------------------ Settings Name Header ------------------------------'
        '//Setting Header Text Movie
        grid_movie.Columns(0).HeaderText = "Id"
        grid_movie.Columns(1).HeaderText = "Movie"
        grid_movie.Columns(2).HeaderText = "Ticket Price"
        grid_movie.Columns(3).HeaderText = "Total Boking"
        grid_movie.Columns(4).Visible = False '//Tanggal insert

        '//Setting Header Boking
        grid_bockingtickets.Columns(0).HeaderText = "Id"
        grid_bockingtickets.Columns(1).HeaderText = "Name"
        grid_bockingtickets.Columns(2).HeaderText = "Gender"
        grid_bockingtickets.Columns(3).HeaderText = "Phone Number"
        grid_bockingtickets.Columns(4).HeaderText = "Movie Booking"
        grid_bockingtickets.Columns(5).HeaderText = "Snack"
        grid_bockingtickets.Columns(6).HeaderText = "Price"
        grid_bockingtickets.Columns(7).Visible = False '//Insert Data


        '//Setting Customer Data
        grid_costumer.Columns(0).HeaderText = "ID CUSTOMER"
        grid_costumer.Columns(1).HeaderText = "NAME"
        grid_costumer.Columns(2).HeaderText = "GENDER"
        grid_costumer.Columns(3).HeaderText = "PHONE NUMBER"
        grid_costumer.Columns(4).Visible = False '//Movie Booking
        grid_costumer.Columns(5).Visible = False '//Movie Snack
        grid_costumer.Columns(6).Visible = False '//Movie Price
        'grid_costumer.Columns(7).Visible = False '//Insert Data
        grid_costumer.Columns(7).HeaderText = "REGISTRATION" '//Insert Data
    End Sub
    Public Sub refreshdataload()
        grid_movie.DataSource = CRUD.View("tblmovie")
        grid_bockingtickets.DataSource = CRUD.View("tbl_book")


        Using merg As New DataTable
            merg.Merge(grid_bockingtickets.DataSource)
            grid_costumer.DataSource = merg
        End Using
    End Sub

    Private Sub homepage_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If MessageBox.Show(
                           Me, "Logout..?", "", MessageBoxButtons.YesNo,
                           MessageBoxIcon.Question
                           ) = Windows.Forms.DialogResult.Yes Then

            'Close
            modfunction.MetroFrameworkShowClose(Login, Me)
        Else
            e.Cancel = True
        End If
    End Sub
End Class
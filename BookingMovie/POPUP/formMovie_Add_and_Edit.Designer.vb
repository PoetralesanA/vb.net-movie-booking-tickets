﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    <Obsolete>
Partial Class formMovie_Add_and_Edit
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formMovie_Add_and_Edit))
        Me.txtIdmovie = New MetroFramework.Controls.MetroTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.MetroButton1 = New MetroFramework.Controls.MetroButton()
        Me.txtPrice = New MetroFramework.Controls.MetroTextBox()
        Me.txtMovieName = New MetroFramework.Controls.MetroTextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtIdmovie
        '
        Me.txtIdmovie.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.txtIdmovie.CustomButton.Image = Nothing
        Me.txtIdmovie.CustomButton.Location = New System.Drawing.Point(108, 1)
        Me.txtIdmovie.CustomButton.Name = ""
        Me.txtIdmovie.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtIdmovie.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtIdmovie.CustomButton.TabIndex = 1
        Me.txtIdmovie.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtIdmovie.CustomButton.UseSelectable = True
        Me.txtIdmovie.CustomButton.Visible = False
        Me.txtIdmovie.Lines = New String(-1) {}
        Me.txtIdmovie.Location = New System.Drawing.Point(15, 34)
        Me.txtIdmovie.MaxLength = 32767
        Me.txtIdmovie.Name = "txtIdmovie"
        Me.txtIdmovie.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtIdmovie.PromptText = "Id Movie.."
        Me.txtIdmovie.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtIdmovie.SelectedText = ""
        Me.txtIdmovie.SelectionLength = 0
        Me.txtIdmovie.SelectionStart = 0
        Me.txtIdmovie.ShortcutsEnabled = True
        Me.txtIdmovie.Size = New System.Drawing.Size(130, 23)
        Me.txtIdmovie.TabIndex = 0
        Me.txtIdmovie.Tag = "checkvalue"
        Me.txtIdmovie.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.txtIdmovie.UseSelectable = True
        Me.txtIdmovie.WaterMark = "Id Movie.."
        Me.txtIdmovie.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtIdmovie.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.MetroButton1)
        Me.GroupBox1.Controls.Add(Me.txtPrice)
        Me.GroupBox1.Controls.Add(Me.txtMovieName)
        Me.GroupBox1.Controls.Add(Me.txtIdmovie)
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(12, 89)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(348, 235)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Add Movie"
        '
        'MetroButton1
        '
        Me.MetroButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroButton1.Location = New System.Drawing.Point(15, 187)
        Me.MetroButton1.Name = "MetroButton1"
        Me.MetroButton1.Size = New System.Drawing.Size(107, 30)
        Me.MetroButton1.TabIndex = 3
        Me.MetroButton1.Text = "Save >>"
        Me.MetroButton1.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroButton1.UseSelectable = True
        '
        'txtPrice
        '
        '
        '
        '
        Me.txtPrice.CustomButton.Image = Nothing
        Me.txtPrice.CustomButton.Location = New System.Drawing.Point(243, 1)
        Me.txtPrice.CustomButton.Name = ""
        Me.txtPrice.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtPrice.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtPrice.CustomButton.TabIndex = 1
        Me.txtPrice.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtPrice.CustomButton.UseSelectable = True
        Me.txtPrice.CustomButton.Visible = False
        Me.txtPrice.Lines = New String(-1) {}
        Me.txtPrice.Location = New System.Drawing.Point(15, 135)
        Me.txtPrice.MaxLength = 32767
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPrice.PromptText = "Price..."
        Me.txtPrice.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtPrice.SelectedText = ""
        Me.txtPrice.SelectionLength = 0
        Me.txtPrice.SelectionStart = 0
        Me.txtPrice.ShortcutsEnabled = True
        Me.txtPrice.Size = New System.Drawing.Size(265, 23)
        Me.txtPrice.TabIndex = 2
        Me.txtPrice.Tag = "checkvalue"
        Me.txtPrice.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.txtPrice.UseSelectable = True
        Me.txtPrice.WaterMark = "Price..."
        Me.txtPrice.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtPrice.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txtMovieName
        '
        '
        '
        '
        Me.txtMovieName.CustomButton.Image = Nothing
        Me.txtMovieName.CustomButton.Location = New System.Drawing.Point(243, 1)
        Me.txtMovieName.CustomButton.Name = ""
        Me.txtMovieName.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtMovieName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtMovieName.CustomButton.TabIndex = 1
        Me.txtMovieName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtMovieName.CustomButton.UseSelectable = True
        Me.txtMovieName.CustomButton.Visible = False
        Me.txtMovieName.Lines = New String(-1) {}
        Me.txtMovieName.Location = New System.Drawing.Point(15, 95)
        Me.txtMovieName.MaxLength = 32767
        Me.txtMovieName.Name = "txtMovieName"
        Me.txtMovieName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtMovieName.PromptText = "Movie Name...."
        Me.txtMovieName.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtMovieName.SelectedText = ""
        Me.txtMovieName.SelectionLength = 0
        Me.txtMovieName.SelectionStart = 0
        Me.txtMovieName.ShortcutsEnabled = True
        Me.txtMovieName.Size = New System.Drawing.Size(265, 23)
        Me.txtMovieName.TabIndex = 1
        Me.txtMovieName.Tag = "checkvalue"
        Me.txtMovieName.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.txtMovieName.UseSelectable = True
        Me.txtMovieName.WaterMark = "Movie Name...."
        Me.txtMovieName.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtMovieName.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(97, 23)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(37, 27)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 13
        Me.PictureBox2.TabStop = False
        '
        'formMovie_Add_and_Edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(374, 347)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "formMovie_Add_and_Edit"
        Me.Opacity = 0.98R
        Me.Text = "Movie"
        Me.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtIdmovie As MetroFramework.Controls.MetroTextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtMovieName As MetroFramework.Controls.MetroTextBox
    Friend WithEvents MetroButton1 As MetroFramework.Controls.MetroButton
    Friend WithEvents txtPrice As MetroFramework.Controls.MetroTextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class

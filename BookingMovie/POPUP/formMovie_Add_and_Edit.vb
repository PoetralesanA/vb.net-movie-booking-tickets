﻿Public Class formMovie_Add_and_Edit

    Private Sub ButtonEditSaveData(sender As Object, e As EventArgs) Handles MetroButton1.Click
        '//Check value null
        Select Case modfunction.CheckTextKosong(GroupBox1, "checkvalue")
            Case False
                MessageBox.Show("The data you filled in is incomplete!", "failed",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Case True

                '//CheckText Button
                Select Case MetroButton1.Text
                    Case "Save >>"
                        MovieInsert()
                        modfunction.ClearAllControls(GroupBox1, "checkvalue")
                    Case "Update Data"
                        MovieUpdate()
                End Select
        End Select
    End Sub
    Private Sub txtPrice_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrice.KeyPress
        modfunction.NumbericOnly(e)
    End Sub
#Region "Insert Update Data Method"
    Private Sub MovieInsert()
        Dim query As String = String.Empty
        query = "INSERT INTO `tblmovie`(`idmovie`, `movie`, `price`, `book_count`, `date`) VALUES ('" &
               txtIdmovie.Text & "','" & StrConv(txtMovieName.Text, VbStrConv.ProperCase) & "','" & txtPrice.Text & "','" & 0 & "','" & modfunction.InsertDateToDB(Now.Date) & "')"

        Select Case CRUD.Save_Data(query)
            Case True
                '//refresh data
                homepage.refreshdataload()

                '//Showmessages
                MessageBox.Show(Me, "Data successfully added", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Case False
                MessageBox.Show(Me, "Failed to add data!!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Select
    End Sub
    Private Sub MovieUpdate()
        If MessageBox.Show(Me, "Update Data?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            Dim queryupdate As String = String.Empty



            queryupdate = "UPDATE `tblmovie` SET `movie`='" & txtMovieName.Text & "',`price`='" & txtPrice.Text & "' WHERE `idmovie`='" & txtIdmovie.Text & "'"
            Select Case CRUD.Update_Data(queryupdate)
                Case True
                    '//refresh data
                    homepage.refreshdataload()
                    MessageBox.Show(Me, "Update Successfully..", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Case False
                    MessageBox.Show(Me, "Failed to update data!!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            End Select
        End If
    End Sub
#End Region
End Class
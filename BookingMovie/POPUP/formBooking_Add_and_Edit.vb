﻿Public Class formBooking_Add_and_Edit
    Dim queryinsert As String, queryupdate As String
    Dim getItemGender As String = String.Empty
    Private Sub MetroButton1_Click(sender As Object, e As EventArgs) Handles MetroButton1.Click
        '//Validasi Empty Value
        Select Case modfunction.CheckTextKosong(GroupBox1, "checkvalue")
            Case False
                MessageBox.Show("The data you filled in is incomplete!", "failed",
                                MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Case True
                Select Case MetroButton1.Text
                    Case "Save >>"
                        '//Boking Tickets
                        B_Tickets()
                    Case "Update Data"
                        '//Update Ticket
                        U_Tickets()
                End Select
        End Select
    End Sub

    '//Bocking Movie Tickets
    Private Sub B_Tickets()

        queryinsert = "INSERT INTO `tbl_book`(`idbook`, `booking_name`, `gender`, `phone`, `moviename`, `food`, `price`, `date`) VALUES ('" &
                      txtIdbocking.Text & "','" & StrConv(txtName.Text, VbStrConv.Uppercase) & "','" & getItemGender & "','" & txtPhone.Text & "','" & cmbMoviename.Text & "','" &
                      cmbSnack.Text & "','" & lbl_price.Text & "','" & InsertDateToDB(Now.Date) & "')"
        Select Case CRUD.Save_Data(queryinsert)
            Case True
                bookingtickets.Update()
                homepage.refreshdataload()
                MessageBox.Show(Me, "Data successfully added", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Case False
                MessageBox.Show(Me, "Failed to add data!!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Select
    End Sub
    '//Update Movie Tickets
    Private Sub U_Tickets()
        queryupdate = String.Empty

        If MessageBox.Show(Me, "Update Data?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            queryupdate = "UPDATE `tbl_book` SET `booking_name`='" & txtName.Text &
                "',`gender`='" & getItemGender &
                "',`phone`='" & txtPhone.Text &
                "',`moviename`='" & cmbMoviename.Text &
                "',`food`='" & cmbSnack.Text &
                "',`price`='" & lbl_price.Text &
                "' WHERE `idbook`='" & txtIdbocking.Text & "'"
            Select Case CRUD.Update_Data(queryupdate)
                Case True
                    '//refresh data
                    homepage.refreshdataload()
                    MessageBox.Show(Me, "Update Successfully..", "", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Case False
                    MessageBox.Show(Me, "Failed to update data!!", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            End Select
        End If

    End Sub
    Private Sub rbMale_CheckedChanged(sender As Object, e As EventArgs) Handles rbMale.CheckedChanged
        getItemGender = rbMale.Text
    End Sub

    Private Sub rbFemale_CheckedChanged(sender As Object, e As EventArgs) Handles rbFemale.CheckedChanged
        getItemGender = rbFemale.Text
    End Sub
    Private Sub txtPhone_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPhone.KeyPress
        modfunction.NumbericOnly(e)
    End Sub

    Private Sub formBooking_Add_and_Edit_Load(sender As Object, e As EventArgs) Handles Me.Load
        LoadDataCombobox()
        cmbMoviename.SelectedItem = homepage.movienamePUBLIC
    End Sub
    '//take movie data on DataGrid
    Private Sub LoadDataCombobox()
        For Each item As DataGridViewRow In homepage.grid_movie.Rows
            If item.Cells.Count >= 1 AndAlso item.Cells(1).Value IsNot Nothing Then
                cmbMoviename.Items.Add(item.Cells(1).Value.ToString())
            End If
        Next
    End Sub
    Private Sub cmbMoviename_SelectedValueChanged(sender As Object, e As EventArgs) Handles cmbMoviename.SelectedValueChanged
        '//Show Total Price
        lbl_price.Text = CRUD.SelectDataTableWHERE("tblmovie", "movie", cmbMoviename.Text, "price")
    End Sub

    Private Sub formBooking_Add_and_Edit_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        homepage.movienamePUBLIC = String.Empty
    End Sub
End Class
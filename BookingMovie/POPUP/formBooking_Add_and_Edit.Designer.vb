﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<Obsolete>
Partial Class formBooking_Add_and_Edit
    Inherits MetroFramework.Forms.MetroForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(formBooking_Add_and_Edit))
        Me.txtIdbocking = New MetroFramework.Controls.MetroTextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lbl_price = New MetroFramework.Controls.MetroLabel()
        Me.MetroLabel1 = New MetroFramework.Controls.MetroLabel()
        Me.cmbSnack = New MetroFramework.Controls.MetroComboBox()
        Me.cmbMoviename = New MetroFramework.Controls.MetroComboBox()
        Me.rbFemale = New MetroFramework.Controls.MetroRadioButton()
        Me.rbMale = New MetroFramework.Controls.MetroRadioButton()
        Me.MetroButton1 = New MetroFramework.Controls.MetroButton()
        Me.txtPhone = New MetroFramework.Controls.MetroTextBox()
        Me.txtName = New MetroFramework.Controls.MetroTextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtIdbocking
        '
        Me.txtIdbocking.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        '
        '
        '
        Me.txtIdbocking.CustomButton.Image = Nothing
        Me.txtIdbocking.CustomButton.Location = New System.Drawing.Point(108, 1)
        Me.txtIdbocking.CustomButton.Name = ""
        Me.txtIdbocking.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtIdbocking.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtIdbocking.CustomButton.TabIndex = 1
        Me.txtIdbocking.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtIdbocking.CustomButton.UseSelectable = True
        Me.txtIdbocking.CustomButton.Visible = False
        Me.txtIdbocking.Lines = New String(-1) {}
        Me.txtIdbocking.Location = New System.Drawing.Point(15, 34)
        Me.txtIdbocking.MaxLength = 32767
        Me.txtIdbocking.Name = "txtIdbocking"
        Me.txtIdbocking.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtIdbocking.PromptText = "Id Booking"
        Me.txtIdbocking.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtIdbocking.SelectedText = ""
        Me.txtIdbocking.SelectionLength = 0
        Me.txtIdbocking.SelectionStart = 0
        Me.txtIdbocking.ShortcutsEnabled = True
        Me.txtIdbocking.Size = New System.Drawing.Size(130, 23)
        Me.txtIdbocking.TabIndex = 0
        Me.txtIdbocking.Tag = "checkvalue"
        Me.txtIdbocking.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.txtIdbocking.UseSelectable = True
        Me.txtIdbocking.WaterMark = "Id Booking"
        Me.txtIdbocking.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtIdbocking.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lbl_price)
        Me.GroupBox1.Controls.Add(Me.MetroLabel1)
        Me.GroupBox1.Controls.Add(Me.cmbSnack)
        Me.GroupBox1.Controls.Add(Me.cmbMoviename)
        Me.GroupBox1.Controls.Add(Me.rbFemale)
        Me.GroupBox1.Controls.Add(Me.rbMale)
        Me.GroupBox1.Controls.Add(Me.MetroButton1)
        Me.GroupBox1.Controls.Add(Me.txtPhone)
        Me.GroupBox1.Controls.Add(Me.txtName)
        Me.GroupBox1.Controls.Add(Me.txtIdbocking)
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(23, 82)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(601, 372)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data"
        '
        'lbl_price
        '
        Me.lbl_price.BackColor = System.Drawing.Color.Indigo
        Me.lbl_price.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.lbl_price.FontWeight = MetroFramework.MetroLabelWeight.Bold
        Me.lbl_price.ForeColor = System.Drawing.Color.Red
        Me.lbl_price.Location = New System.Drawing.Point(385, 271)
        Me.lbl_price.Name = "lbl_price"
        Me.lbl_price.Size = New System.Drawing.Size(193, 82)
        Me.lbl_price.TabIndex = 10
        Me.lbl_price.Text = "000000"
        Me.lbl_price.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lbl_price.UseCustomBackColor = True
        Me.lbl_price.UseCustomForeColor = True
        '
        'MetroLabel1
        '
        Me.MetroLabel1.AutoSize = True
        Me.MetroLabel1.BackColor = System.Drawing.Color.Lime
        Me.MetroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall
        Me.MetroLabel1.Location = New System.Drawing.Point(385, 246)
        Me.MetroLabel1.Name = "MetroLabel1"
        Me.MetroLabel1.Size = New System.Drawing.Size(85, 25)
        Me.MetroLabel1.TabIndex = 9
        Me.MetroLabel1.Text = "P R I C E :"
        Me.MetroLabel1.UseCustomBackColor = True
        '
        'cmbSnack
        '
        Me.cmbSnack.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbSnack.FormattingEnabled = True
        Me.cmbSnack.ItemHeight = 23
        Me.cmbSnack.Items.AddRange(New Object() {"Popcorn", "French Fries", "Hot Dog", "Fish Ball", "Nachos", "Coklat", "Nugget"})
        Me.cmbSnack.Location = New System.Drawing.Point(15, 221)
        Me.cmbSnack.Name = "cmbSnack"
        Me.cmbSnack.PromptText = "Snack..."
        Me.cmbSnack.Size = New System.Drawing.Size(265, 29)
        Me.cmbSnack.TabIndex = 5
        Me.cmbSnack.Tag = "checkvalue"
        Me.cmbSnack.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.cmbSnack.UseSelectable = True
        '
        'cmbMoviename
        '
        Me.cmbMoviename.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.cmbMoviename.FormattingEnabled = True
        Me.cmbMoviename.ItemHeight = 23
        Me.cmbMoviename.Location = New System.Drawing.Point(15, 271)
        Me.cmbMoviename.Name = "cmbMoviename"
        Me.cmbMoviename.PromptText = "Movie Name...."
        Me.cmbMoviename.Size = New System.Drawing.Size(306, 29)
        Me.cmbMoviename.TabIndex = 6
        Me.cmbMoviename.Tag = "checkvalue"
        Me.cmbMoviename.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.cmbMoviename.UseSelectable = True
        '
        'rbFemale
        '
        Me.rbFemale.AutoSize = True
        Me.rbFemale.ForeColor = System.Drawing.Color.Transparent
        Me.rbFemale.Location = New System.Drawing.Point(93, 124)
        Me.rbFemale.Name = "rbFemale"
        Me.rbFemale.Size = New System.Drawing.Size(61, 15)
        Me.rbFemale.TabIndex = 3
        Me.rbFemale.Tag = ""
        Me.rbFemale.Text = "Female"
        Me.rbFemale.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.rbFemale.UseSelectable = True
        '
        'rbMale
        '
        Me.rbMale.AutoSize = True
        Me.rbMale.ForeColor = System.Drawing.Color.Transparent
        Me.rbMale.Location = New System.Drawing.Point(15, 124)
        Me.rbMale.Name = "rbMale"
        Me.rbMale.Size = New System.Drawing.Size(49, 15)
        Me.rbMale.TabIndex = 2
        Me.rbMale.Tag = ""
        Me.rbMale.Text = "Male"
        Me.rbMale.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.rbMale.UseSelectable = True
        '
        'MetroButton1
        '
        Me.MetroButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.MetroButton1.Location = New System.Drawing.Point(15, 323)
        Me.MetroButton1.Name = "MetroButton1"
        Me.MetroButton1.Size = New System.Drawing.Size(164, 30)
        Me.MetroButton1.TabIndex = 7
        Me.MetroButton1.Text = "Save >>"
        Me.MetroButton1.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.MetroButton1.UseSelectable = True
        '
        'txtPhone
        '
        '
        '
        '
        Me.txtPhone.CustomButton.Image = Nothing
        Me.txtPhone.CustomButton.Location = New System.Drawing.Point(301, 1)
        Me.txtPhone.CustomButton.Name = ""
        Me.txtPhone.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtPhone.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtPhone.CustomButton.TabIndex = 1
        Me.txtPhone.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtPhone.CustomButton.UseSelectable = True
        Me.txtPhone.CustomButton.Visible = False
        Me.txtPhone.Lines = New String(-1) {}
        Me.txtPhone.Location = New System.Drawing.Point(15, 165)
        Me.txtPhone.MaxLength = 32767
        Me.txtPhone.Name = "txtPhone"
        Me.txtPhone.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtPhone.PromptText = "Phone Number..."
        Me.txtPhone.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtPhone.SelectedText = ""
        Me.txtPhone.SelectionLength = 0
        Me.txtPhone.SelectionStart = 0
        Me.txtPhone.ShortcutsEnabled = True
        Me.txtPhone.Size = New System.Drawing.Size(323, 23)
        Me.txtPhone.TabIndex = 4
        Me.txtPhone.Tag = "checkvalue"
        Me.txtPhone.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.txtPhone.UseSelectable = True
        Me.txtPhone.WaterMark = "Phone Number..."
        Me.txtPhone.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtPhone.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.CustomButton.Image = Nothing
        Me.txtName.CustomButton.Location = New System.Drawing.Point(301, 1)
        Me.txtName.CustomButton.Name = ""
        Me.txtName.CustomButton.Size = New System.Drawing.Size(21, 21)
        Me.txtName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
        Me.txtName.CustomButton.TabIndex = 1
        Me.txtName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
        Me.txtName.CustomButton.UseSelectable = True
        Me.txtName.CustomButton.Visible = False
        Me.txtName.Lines = New String(-1) {}
        Me.txtName.Location = New System.Drawing.Point(15, 95)
        Me.txtName.MaxLength = 32767
        Me.txtName.Name = "txtName"
        Me.txtName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.txtName.PromptText = "Name...."
        Me.txtName.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.txtName.SelectedText = ""
        Me.txtName.SelectionLength = 0
        Me.txtName.SelectionStart = 0
        Me.txtName.ShortcutsEnabled = True
        Me.txtName.Size = New System.Drawing.Size(323, 23)
        Me.txtName.TabIndex = 1
        Me.txtName.Tag = "checkvalue"
        Me.txtName.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.txtName.UseSelectable = True
        Me.txtName.WaterMark = "Name...."
        Me.txtName.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.txtName.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(184, 25)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(37, 27)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 14
        Me.PictureBox2.TabStop = False
        '
        'formBooking_Add_and_Edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(647, 492)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(647, 492)
        Me.MinimizeBox = False
        Me.Name = "formBooking_Add_and_Edit"
        Me.Opacity = 0.98R
        Me.Style = MetroFramework.MetroColorStyle.Teal
        Me.Text = "Ticket Booking"
        Me.Theme = MetroFramework.MetroThemeStyle.Dark
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtIdbocking As MetroFramework.Controls.MetroTextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents MetroButton1 As MetroFramework.Controls.MetroButton
    Friend WithEvents txtPhone As MetroFramework.Controls.MetroTextBox
    Friend WithEvents txtName As MetroFramework.Controls.MetroTextBox
    Friend WithEvents rbMale As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents rbFemale As MetroFramework.Controls.MetroRadioButton
    Friend WithEvents cmbSnack As MetroFramework.Controls.MetroComboBox
    Friend WithEvents cmbMoviename As MetroFramework.Controls.MetroComboBox
    Friend WithEvents lbl_price As MetroFramework.Controls.MetroLabel
    Friend WithEvents MetroLabel1 As MetroFramework.Controls.MetroLabel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
End Class
